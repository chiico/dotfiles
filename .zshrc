# Path to dotfiles
export DOTFILES=$HOME/Developer/.dotfiles

# Path to oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Enable completions
autoload -Uz compinit && compinit

# Minimal theme settings
export MNML_INSERT_CHAR="$"
export MNML_PROMPT=(mnml_git mnml_keymap)
export MNML_RPROMPT=('mnml_cwd 20')

# Set ZSH theme
ZSH_THEME="minimal"

# Language Environment
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# Plugins
plugins=(git)

source $ZSH/oh-my-zsh.sh
